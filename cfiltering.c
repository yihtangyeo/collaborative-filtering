/*
Name: YEO, YIH TANG
CSC192 LEC0101 TUT0101
Assignment 2
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_USERS 9
#define MAX_MOVIES 9

/* function that is used to calculate Euclidean distance, by passing the array into the function
with the user index numbers, and will return the distance in floating point variable.*/
float compareUsers(int db[][9], int index1, int index2);

int main()
{
    FILE *inputFile;
    char fileName[100];                     // user input filename to be read

    char currentChar;                       // used to store char when data is streamed from file when calling fgetc()
    int currentInt;                         // used to store int when data is read from file when calling atoi()
    char currentString[3];                  // used to store string when data is read from file when calling fgets(), make it to have a length of 3 because I are reading 3 letters, which is a number and '\n' and the '\0'

    int arrayInput[MAX_USERS][MAX_MOVIES] = {{0}};              // analyse and store inputFile into array form, initializes it to 0
    float arrayOutput[MAX_USERS][MAX_MOVIES] = {{0.0f}};        // array used to store the result of the calculated distance between users
    int numberOfUsers;                                          // variable that stores the first character in the inputFile about the number of users
    int numberOfMovies;                                         // variable that stores the second character in the  inputFile about the number of movies

    int row = 0, column = 0;                // counter used when reading the data from inputFile and to store it into 2D arrayInput (more explanation below)
    int i = 0, j = 0;                       // counter used for for-loop

    // ask users to type in the name of the file that they want to analyze
    printf("Enter the file name to be read: ");
    scanf("%s", fileName);

    // attempt to open the file to be read "r"
    inputFile = fopen(fileName, "r");

    // Error Detection: if the file is empty (does not exist), terminate the program immediately, if not, proceed to analyzing the inputFile
    if (inputFile == NULL){
        printf("Sorry, file cannot be opened or is not present.\n");
        return 0;
    }
    else{

        /*
        a correct inputFile should have this format: the first line containing the number of users, therefore, get the first line of inputFile
        using fgets() and do the same for second line of inputFile that contains the number of movies, and then store them respectively into
        the variables numberOfUsers and numberOfMovies. Because these two lines are almost identical, we can use a for loop to simplify code
        */

        for (i = 0; i < 2; i++){

            // get 3 characters from a line of string from the inputFile
            fgets(currentString, 3, inputFile);

            // convert the first character of the string (that should have contained an integer) into an integer
            currentInt = atoi(&currentString[0]);

            // Error Detection: if the value of the numberOfUsers or numberOfMovies is less than zero, or more than 9, terminate the program
            if (currentInt > 0 && currentInt < 10){
                // this line is a simplify if-else statement - (is i = 0 (which implies that the first line))? (if yes, store it into numberOfUsers):(if not, store it into numberOfMovies)
                (i == 0)? (numberOfUsers = currentInt) : (numberOfMovies = currentInt);
            }
            else{
                printf("The file is not properly formatted to be analyzed.\n");
                return 0;
            }
        }

        // Error Detection: check if the number of movies and number of users are within the range of 3-9, if not, terminate the program
        if (numberOfMovies > 9 || numberOfMovies < 3 || numberOfUsers > 9 || numberOfUsers < 3){
            printf("The volume of data is beyond the range that the program can process.\n");
            return 0;
        }

        // because the next line should be blank, we get that line and analyze if it is really a blank line
        fgets(currentString, 3, inputFile);

        // Error Detection: if it is not a blank line but something not matching the input, terminate the program
        if (currentString[0] != '\n'){
            printf("The file is not properly formatted to be analyzed.\n");
            return 0;
        }


        /*
        by using a do-while loop, ask the program to read the remaining of the inputFile until EOF (End Of File). During the process of reading the file,
        analyze the character - if it is a ' ' space skip that, if it is a number attempt to read that, and store it into the respective position in the array.
        The way we determine which position to store in the array is to have two variables, row and column, which are both initialized to zero. As a number is
        passed into the array, we increment the column. When a newline character '\n' is found, we reset the column to zero, while incrementing the row.
        Note that this program doesn't care about having characters such as 'a', 'b', 'c', etc because when they are converted to int, they will hold a value
        of 0, and therefore will be detected as an error as determined by the code below (more explanation below)
        */

        do{
            // read character by character from the inputFile
            currentChar =  (fgetc(inputFile));

            // if the character is not a space or newline, store it into arrayInput
            if (currentChar != ' ' && currentChar != '\n'){

                // convert char into int using atoi()
                currentInt = atoi(&currentChar);

                // store it into array
                arrayInput[row][column] = currentInt;

                // increment the column count so that the next value will go into a new position of the array
                column++;
            }
            // if it is a newline character
            else if (currentChar == '\n'){
                // reset the column to zero
                column = 0;
                // increment the row count so that new set of values will go into a new row
                row++;
            }

        } while(currentChar != EOF);
        // when the file reaches the end, terminate the loop.

        /*
        The code below is used to verify the arrayInput:
        - to check if there is foreign characters beyond the range of 1-5
        - to check if the inputFile header says there is 'n' number of users and 'n' number of movies but the arrayInput doesn't agree with that
        Algorithm used:
        Using a for loop, check all 81 elements in the 9x9 array, those within (numberOfUsers)x(numberOfMovies) should not have a value of zero,
        because the range of values permitted is 1-5. This will help to detect error especially if the inputFile has characters rather than numbers.
        Outside the area of (numberOfUsers)x(numberOfMovies), all the values should be zero, if not, it is an invalid array.
        */
        for (i = 0; i < MAX_USERS; i++){
            for (j = 0; j < MAX_MOVIES; j++){

                // those within the range of (numberOfUsers)x(numberOfMovies) should NOT have a value of zero, if not, terminate the program
                if (i < numberOfUsers && j < numberOfMovies){
                    if (arrayInput[i][j] < 1 || arrayInput[i][j] > 5){
                        printf("The file is not properly formatted to be analyzed.\n");
                        return 0;
                    }
                }
                else{
                    // those beyond the range of (numberOfUsers)x(numberOfMovies) should have a value of zero, if not, terminate the program
                    if (arrayInput[i][j] != 0){
                        printf("tThe file is not properly formatted to be analyzed.\n");
                        return 0;
                    }
                }

            }
        }

        // Using a for loop, print of the inputFile that is analyzed into an array
        for (i = 0; i < numberOfUsers; i++){
            for (j = 0; j < numberOfMovies; j++){
                printf("%d ", arrayInput[i][j]);
            }
            printf("\n");
        }
        // Show that number of users and the number of movies
        printf("Number of users: %d\nNumber of movies: %d\n\n", numberOfUsers, numberOfMovies);

        /*
        Analysis part of the code:
        using a for loop, passes the array and the user index that will be compared into a function that will return the distance between the users
        - the for loop basically does something like this: 0 compare with 0, 0 compare with 1, 0 compare with 2.. until 0 compare (numberOfUsers),
        and then goes 1 compare 1, 1 compare with 2, up to 1 compare (numberOfUsers)... this process repeats.
        - this code does not try to compare backward, which is to do 0 compare with 1 and 1 compare with 0 again, because both will return the SAME
        result, I choose to have arrayOutput[i][j] = arrayOutput[j][i], which will reduce the number of iteration required
        */
        for (i = 0; i < numberOfUsers; i++){
            for (j = i; j < numberOfUsers; j++){
                // Function call: calculate the distance between the two users specified by the for loop
                arrayOutput[i][j] = compareUsers(arrayInput, i, j);
                arrayOutput[j][i] = arrayOutput[i][j];
            }
        }

        // print out the arrayOutput that contains the distance between the users in a chart format
        for (i = 0; i < numberOfUsers; i++){
            for (j = 0; j < numberOfUsers; j++){
                printf("%.4f ", arrayOutput[i][j]);
            }
            printf("\n");
        }
    }
    // after finish using the inputFile, close it.
    fclose(inputFile);
    return 0;
}

// function prototype
float compareUsers(int db[][9], int index1, int index2){

    // function definition

    int k = 0;                              // variable that is used as counter in for loop
    float totalNum = 0.0f;                  // the totalSum of square of the difference in each movie for the two users
    float distanceNum = 0.0f;               // the distance after performing square root of totalNum

    // for loop that counts the square of the difference between two rows (users)
    for (k = 0; k < MAX_MOVIES; k++){
        totalNum += (db[index1][k]-db[index2][k])*(db[index1][k]-db[index2][k]);
    }

    // using math.h square root function, find the square root of totalNum
    distanceNum = sqrt(totalNum);

    // using the formula to calculate the reciprocal of distance, 1 is added to prevent dividing by zero
    distanceNum = 1/(1+distanceNum);

    // return the distanceNum which contains a floating point number.
    return distanceNum;
}
